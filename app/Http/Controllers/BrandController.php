<?php

namespace App\Http\Controllers;

use App\Http\Requests\BrandRequest;
use App\Models\Brand;
use Illuminate\Http\Request;
use Illuminate\Database\QueryException;

class BrandController extends Controller
{
    public function index()
    {
        $brands = Brand::latest()->get();
        return view('backend.brands.index', [
            'brands' => $brands
        ]);
    }
    public function create()
    {
        return view('backend.brands.create');
    }
    public function store(BrandRequest $request)
    {
        try{
            Brand::create([
                'title'=>$request->title,
                'description'=>$request->description,
            ]);
            return redirect()->route('brands')->withMessage('Brand Successfully Added'); 
        } catch(QueryException $e){
            return redirect()->back()->withInput()->withError($e->getMessage());
        }
         
    }
    public function show(Brand $brand)
    {
        return view('backend.brands.show', [
            'brand' => $brand
        ]);
    }

    public function edit(Brand $brand)
    {
        return view('backend.brands.edit', [
            'brand' => $brand      
        ]);
    }

    public function update(BrandRequest $request, Brand $brand)
    {
        try{
           $brand->update([
                'title'=>$request->title,
                'description'=>$request->description,
            ]);
            return redirect()->route('brands')->withMessage(' Brand Successfully Updated'); 
        } catch(QueryException $e){
            return redirect()->back()->withInput()->withError($e->getMessage());
        }
    }
    public function destroy(Brand $brand)
    {
        try{
            $brand->delete();
            return redirect()->route('brands')->withMessage('Brand Successfully Deleted'); 
        } catch(QueryException $e){
            return redirect()->back()->withInput()->withError($e->getMessage());
        }
       
    }

}
