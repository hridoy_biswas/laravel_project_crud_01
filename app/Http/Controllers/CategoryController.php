<?php

namespace App\Http\Controllers;

use App\Http\Requests\CategoryRequest;
use App\Models\Category;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    public function index()
    {
        $categories = Category::latest()->get();
        return view('backend.categories.index', [
            'categories' => $categories
        ]);
    }

    public function create()
    {
        return view('backend.categories.create');
    }

    public function store(CategoryRequest $request)
    {
        try{
            // request()->validate([
            //     // 'title'=>['required', 'min:3',Rule::unique('categories','title')],
            //     'title' => 'required|min:3|unique:categories,title',
            //     'description'=>['required', 'min:10'],

            // ]);
            // dd(request()->all());
            Category::create([
                'title'=>$request->title,
                'description'=>$request->description,
    
              
            ]);
            // $request->session()->flash('message','Task is Successfully');
            return redirect()->route('categories')->withMessage('Task is Successfully'); 
        } catch(QueryException $e){
            return redirect()->back()->withInput()->withError($e->getMessage());
        }
         
    }

    public function show(Category $category)
    {
        // $category = Category::find($id);
        return view('backend.categories.show', [
            'category' => $category
        ]);
        // dd($category);
    }

    public function edit(Category $category)
    {
        return view('backend.categories.edit', [
            'category' => $category      
        ]);
    }

    public function update(CategoryRequest $request, Category $category)
    {
        try{
           $category->update([
                'title'=>$request->title,
                'description'=>$request->description,
    
              
            ]);
            // $request->session()->flash('message','Task is Successfully');
            return redirect()->route('categories')->withMessage('Successfully Updated'); 
        } catch(QueryException $e){
            return redirect()->back()->withInput()->withError($e->getMessage());
        }
    }
    public function destroy(Category $category)
    {
        try{
            $category->delete();
            return redirect()->route('categories')->withMessage('Deleted Successfully'); 
        } catch(QueryException $e){
            return redirect()->back()->withInput()->withError($e->getMessage());
        }
       
    }
}
