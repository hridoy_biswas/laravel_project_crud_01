<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class BrandRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            // 'title' => 'required|min:3|unique:brands,title,'.$this->route('brand')->id,
            'title' => 'required|min:3|unique:brands,title',
            'description'=>['required', 'min:10'],
        ];
    }
}
