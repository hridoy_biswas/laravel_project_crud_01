<x-backend.layouts.master>
    <x-slot name="pageTitle">
        Category
    </x-slot>

    <x-slot name='breadCrumb'>
        <x-backend.layouts.elements.breadcrumb>
            <x-slot name="pageHeader"> Category </x-slot>

            <li class="breadcrumb-item"><a href="index.html">Dashboard</a></li>
            <li class="breadcrumb-item active">Category</li>

        </x-backend.layouts.elements.breadcrumb>
    </x-slot>
    <!-- <h1 class="mt-4">Tables</h1>
        <ol class="breadcrumb mb-4">
            <li class="breadcrumb-item"><a href="index.html">Dashboard</a></li>
            <li class="breadcrumb-item active">Tables</li>
        </ol> -->
    
    <div class="card mb-4">
        <div class="card-body">
            DataTables is a third party plugin that is used to generate the demo table below. For more information about DataTables, please visit the
            <a target="_blank" href="https://datatables.net/">official DataTables documentation</a>
            .
        </div>
    </div>

    <div class="card mb-4">
        <div class="card-header">
            <i class="fas fa-table me-1"></i>
            DataTable Example <a class=" btn btn-sm btn-info" href="{{ route('categories.create') }}">Add New</a>
        </div>
        <div class="card-body">
            @if (session('message'))
            <div class="alert alert-success">
                <span class="close" data-dismiss="alert">&times;</span>
                <strong>{{ session('message') }}.</strong>
            </div>  
            @endif

            <table id="datatablesSimple">
                <thead>
                    <tr>
                        <th>Sl#</th>
                        <th>Title</th>
                        <th>Description</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    @php
                        $sl=0;
                    @endphp
                    @foreach ($categories as $category)
                    <tr>                    
                        <td>{{ ++$sl }}</td> 
                        <td>{{ $category -> title }}</td>
                        <td>{{ $category -> description }}</td>
                       <td>
                           <a class="btn btn-sm btn-info"  href="{{ route('categories.show', ['category' => $category->id]) }}">Show</a>
                           <a class="btn btn-sm btn-warning"  href="{{ route('categories.edit', ['category' => $category->id]) }}">Edit</a>
                           <form style="display:inline" action="{{ route('categories.destroy', ['category' => $category->id]) }}" method="post">
                            @csrf
                            @method('delete')

                            <button onclick="return confirm('Are You Sure Want to Delete')" class="btn btn-sm btn-danger" type="submit">Delete</button>
                        
                            </form>
                           {{-- <a href="{{ route('categories.destroy', ['category' => $category->id]) }}">Delete</a> --}}

                        </td>
                     

                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>

</x-backend.layouts.master>
