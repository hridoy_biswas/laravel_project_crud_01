<x-backend.layouts.master>
    <x-slot name="pageTitle">
        Color
    </x-slot>

    <x-slot name='breadCrumb'>
        <x-backend.layouts.elements.breadcrumb>
            <x-slot name="pageHeader"> Color </x-slot>

            <li class="breadcrumb-item"><a href="index.html">Dashboard</a></li>
            <li class="breadcrumb-item active">Color</li>

        </x-backend.layouts.elements.breadcrumb>
    </x-slot>
    
    <div class="card mb-4">
        <div class="card-body">
            DataTables is a third party plugin that is used to generate the demo table below. For more information about DataTables, please visit the
            <a target="_blank" href="https://datatables.net/">official DataTables documentation</a>
            .
        </div>
    </div>

    <div class="card mb-4">
        <div class="card-header">
            <i class="fas fa-table me-1"></i>
            Product Example <a class=" btn btn-sm btn-info" href="{{ route('color.create') }}">Add New </a>
        </div>
        <div class="card-body">
            @if (session('message'))
            <div class="alert alert-success">
                <span class="close" data-dismiss="alert">&times;</span>
                <strong>{{ session('message') }}.</strong>
            </div>  
            @endif

            <table id="datatablesSimple">
                <thead>
                    <tr>
                        <th>Sl#</th>
                        <th>Title</th>
                        <th>Description</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    @php
                        $sl=0;
                    @endphp
                    @foreach ($brands as $brand)
                    <tr>                    
                        <td>{{ ++$sl }}</td> 
                        <td>{{ $brand->title }}</td>
                        <td>{{ $brand->description }}</td>
                       <td>
                           <a class="btn btn-sm btn-success"  href="{{ route('brands.show', ['brand' => $brand->id]) }}">Show</a>
                           <a class="btn btn-sm btn-warning"  href="{{ route('brands.edit', ['brand' => $brand->id]) }}">Edit</a>
                           <form style="display:inline" action="{{ route('brands.destroy', ['brand' => $brand->id]) }}" method="post">
                            @csrf
                            @method('delete')
                            <button onclick="return confirm('Are You Sure Want to Delete')" class="btn btn-sm btn-danger" type="submit">Delete</button>             
                            </form>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>

</x-backend.layouts.master>
