<x-backend.layouts.master>
    <x-slot name="pageTitle">
       Edit Brand
    </x-slot>

    <x-slot name='breadCrumb'>
        <x-backend.layouts.elements.breadcrumb>
            <x-slot name="pageHeader"> Edit </x-slot>

            <li class="breadcrumb-item"><a href="index.html">Dashboard</a></li>
            <li class="breadcrumb-item active">Edit</li>

        </x-backend.layouts.elements.breadcrumb>
    </x-slot>


    <div  class="card mb-4">
        <div class="card-header">
            <i class="fas fa-table me-1"></i>
           Edit Brand <a class=" btn btn-sm btn-info" href="{{ route('brands') }}">List</a>
        </div>
        <div class="card-body">
            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <form action="{{ route('brands.update', ['brand' => $brand->id]) }}" method="POST">
                @csrf
                @method('patch')
                <div class="form-floating mb-3 mb-md-0">
                    <input class="form-control" value="{{ old('title',$brand->title), }}" name="title" id="inputTitle" type="text" placeholder="Enter Title">
                    <label for="inputTitle">Title</label>
                    @error('title')
                        <span class="small text-danger">{{ $message }}</span>
                    @enderror
                </div>
                <div class="form-floating mt-3">
                    <textarea
                     class="form-control" name="description" id="inputDescription" placeholder="Description">{{ old('description', $brand->description) }}
                    </textarea>
                    <label for="inputDescription">Description</label>
                    @error('description')
                    <span class="small text-danger">{{ $message }}</span>
                @enderror
                </div>
                <div class="mt-4 mb-0">
                   <button type="submit" class="btn btn-primary">
                        Update
                   </button>
                </div>
            </form>
        </div>
    </div>


</x-backend.layouts.master>