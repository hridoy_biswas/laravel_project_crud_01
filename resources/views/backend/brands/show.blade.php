<x-backend.layouts.master>
    <x-slot name="pageTitle">
        Brand Details
    </x-slot>

    <x-slot name='breadCrumb'>
        <x-backend.layouts.elements.breadcrumb>
            <x-slot name="pageHeader"> Brands </x-slot>

            <li class="breadcrumb-item"><a href="#">Dashboard</a></li>
            <li class="breadcrumb-item active">Add New</li>

        </x-backend.layouts.elements.breadcrumb>
    </x-slot>


    <div  class="card mb-4">
        <div class="card-header">
            <i class="fas fa-table me-1"></i>
            Show Brand <a class=" btn btn-sm btn-info" href="{{ route('brands') }}">List</a>
        </div>
        <div class="card-body">
           <h3>Title: {{ $brand->title }}</h3>
           <p>Description: {{ $brand->description }}</p>
        </div>
    </div>


</x-backend.layouts.master>