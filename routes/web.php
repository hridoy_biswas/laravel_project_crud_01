<?php

use App\Http\Controllers\BrandController;
use App\Http\Controllers\CategoryController;
use App\Http\Controllers\ProductController;
use App\Http\Controllers\WelcomeController;
use Illuminate\Support\Facades\Route;

Route::get('/', function(){
    return view('welcome');
});

// Route::get('/home', function(){
//     return view('backend.home');
// });

Route::get('/home', function(){
    return view('backend.home');
});

Route::get('/categories', [CategoryController::class, 'index'])->name('categories');

// Route::get('/form', function(){
//     return view('backend.categories.form');
// })->name('form');


Route::get('/categories/create',[CategoryController::class, 'create'])->name('categories.create');

Route::post('/categories',[CategoryController::class, 'store'])->name('categories.store');

// GET	/photos/{photo}	show	photos.show
Route::get('/categories/{category}',[CategoryController::class, 'show'])->name('categories.show');

// GET	/photos/{photo}/edit	edit	photos.edit
Route::get('/categories/{category}/edit',[CategoryController::class, 'edit'])->name('categories.edit');
// PUT/PATCH	/photos/{photo}	update	photos.update
Route::patch('/categories/{category}',[CategoryController::class, 'update'])->name('categories.update');

// DELETE	/photos/{photo}	destroy	photos.destroy
Route::delete('/categories/{category}',[CategoryController::class, 'destroy'])->name('categories.destroy');



//Brand Route::::::::**********************************************************
Route::get('/brands', [BrandController::class, 'index'])->name('brands');
Route::get('/brands/create',[BrandController::class, 'create'])->name('brands.create');
Route::post('/brands',[BrandController::class, 'store'])->name('brands.store');
Route::get('/brands/{brand}',[BrandController::class, 'show'])->name('brands.show');
Route::get('/brands/{brand}/edit',[BrandController::class, 'edit'])->name('brands.edit');
Route::patch('/brands/{brand}',[BrandController::class, 'update'])->name('brands.update');
Route::delete('/brands/{brand}',[BrandController::class, 'destroy'])->name('brands.destroy');
